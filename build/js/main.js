var mainSlider = new Swiper('.swiper-container', {
    slidesPerView: 1,
    paginationClickable: true,
    spaceBetween:0,
    loop: true,
  autoplay: 4500
});
// Search mob 
$('.btn-mob').on('click', function(){
  $('.form-search').toggleClass('show-search');
})

// TABS
$(document).ready(function(){

    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current-tab');
        $('.tab-content').removeClass('current-tab');

        $(this).addClass('current-tab');
        $("#"+tab_id).addClass('current-tab').fadeIn();
    })

});

// TAbs Master Data
$(document).ready(function(){

    $('ul.tabs2 li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs2 li').removeClass('current-tab2');
        $('.tab-content2').removeClass('current-tab2');

        $(this).addClass('current-tab2');
        $("#"+tab_id).addClass('current-tab2').fadeIn();
    })

});

// SORTING TABLE
$(function(){
  $('#keywords').tablesorter(); 
});
$(function(){
  $('#table-info').tablesorter(); 
});
$(function(){
  $('#history').tablesorter(); 
});