var gulp = require('gulp')
var express = require('express');
var app     = express();
var pug		= require('gulp-pug');
var path		= require('path');
var watch		= require('gulp-watch');
var sass		= require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();

gulp.task('serve', function() {

	app.set('view engine', 'pug');

	app.set('views', path.join(__dirname, '/dev'))

	app.use(express.static(__dirname + '/build'))

	app.get('/:page?', function(req, res) {
		var fileName = req.url.replace(/\//g, '') || 'index'
		if (fileName == 'home' || fileName == 'index') {
			res.render('index')
		} else {
			res.render(fileName)
		}

	})


	var listener    = app.listen(2999);
	var port        = listener.address().port;
	// proxy на локальный сервер на Express
	browserSync.init({
	  proxy: 'http://localhost:'   + port,
	  // startPath: '/build/',
	  notify: false,
	  tunnel: false,
	  host: 'localhost',
	  port: port,
	  logPrefix: 'Proxy to localhost:' + port,
	});
	// обновляем страницу, если был изменен исходник шаблона
	browserSync.watch('./**/*.pug').on('change', browserSync.reload);
})

gulp.task('pug', function () {
	return gulp.src('dev/*.pug')
		.pipe(pug({
			pretty: true
		}))
		.pipe(gulp.dest('build/'))
})

gulp.task('scss', function(){
	return gulp.src('dev/scss/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('build/css/'))
		.pipe(browserSync.reload({stream:true}))
})


gulp.task('watch', function() {
	gulp.watch('dev/scss/**/*.scss', ['scss'], browserSync.reload)
	gulp.watch('build/js/**/*.js', browserSync.reload)
})

gulp.task('default', ['serve', 'scss', 'watch'])
